import json
import boto3
from botocore.exceptions import ClientError
from JpeRestApiLib import *
import base64
import os
import uuid

class api_handler:

    s3 = None
    s3_client = None
    iss = None
    sub = None
    username = None
    root_bucket = None
    response = None
    method = None
    path = None
    dataset_name = None
    catID = None
    zoomlevels = None
    bands = None
    saveLocType = 's3'
    saveLoc = None
    boundBox = None
    session = None
    sceneListDepID = None
    ingestDepID = None
    tilingDepID = None
    purgeDepID = None
    tilingJobUUID = None
    kx_controller = None
    jobIDList = []

    def __init__(self, event, root_bucket):
        print("in api_handler")
        self.root_bucket = root_bucket
        if 'KX_CONTROLLER_IP' in os.environ:
            self.kx_controller = os.environ['KX_CONTROLLER_IP']
        else:
            self.kx_controller = 'http://ec2-52-210-253-209.eu-west-1.compute.amazonaws.com:8080'
        self.process_api_request(event)

    def get_response(self):
        print("in get_responses")
        return self.response


    ############### PRIVATE FUNCTIONS BELOW HERE #################

    ## Extract user pool and user data from token
    def parse_token(self, event):
         # get the token passed to us if it exists
        token = event['headers']['Authorization']
        tokId, tokPay, tokEncSig = token.rsplit('.', 2)
        tokPay += "=" * ((4 - len(tokPay) % 4) % 4)
        decPay = json.loads(base64.b64decode(tokPay))
        tmp, iss = decPay['iss'].rsplit('/',1) # chop off the http://xxx/
        region, tmp = iss.rsplit('_',1)
        self.iss = iss.replace('_', '-')
        self.username = decPay['cognito:username']
        #self.sub = decPay['sub']
        self.sub = '0'    # temporary override until we start using sub id's for folders
        return

    ## Validate if root bucket exists
    def valid_bucket(self):
        bucket = self.s3.Bucket(self.root_bucket)
        exists = True
        try:
            self.s3.meta.client.head_bucket(Bucket=self.root_bucket)
        except ClientError as e:
            # If a client error is thrown, then check that it was a 404 error.
            # If it was a 404 error, then the bucket does not exist.
            error_code = e.response['Error']['Code']
            print(e.response)
            exists = False

        return exists

    def process_api_request(self, event):

        # 3deo-visualizer-resources/eu-west-1-udgY4iHrG/0/visualizer/config/layers

        # Setup s3 client and resource handles
        self.s3 = boto3.resource('s3')
        self.s3_client = boto3.client('s3')

        # User Data from event
        self.parse_token(event)

        # Extract method and path to determine action
        self.method = event['httpMethod']
        self.path = event['path']

        # check root bucket exists, error if not
        if not self.valid_bucket():
            return {  'statusCode': 404, 'body':'No such Bucket'}

        # get list of scenes in each of the sub folders
        statusCode, body = self.do_request(event)

        self.response =  {
            'statusCode': statusCode,
            'body': json.dumps(body)
        }


    def do_request(self, event):
        # Determine Action based on method and path
        if (self.method == 'POST'):

            print(event)

            if (self.path.endswith('generatetileset')):

                self.setupKxSession(event)

                ## Ingest Dataset
                statusCode, body = self.jpeIngest(event, self.path)

                if(statusCode == 200):
                    statusCode, body = self.jpeTile(event)

                if(statusCode == 200):
                    statusCode, body = self.jpePurge(event)

                if(statusCode == 200):
                    jobs = {}
                    jobs['sceneListDepID'] = self.sceneListDepID
                    jobs['ingestDepID'] = self.ingestDepID
                    jobs['tilingDepID'] = self.tilingDepID
                    jobs['purgeDepID'] = self.purgeDepID
                    jobs['tilingJobUUID'] = self.tilingJobUUID
                    return statusCode, jobs
                else:
                    return statusCode, body

        elif (self.method == 'GET'):

            # TODO : required jobID to search for

            self.setupKxSession(event)

            if (self.path.endswith('getjobforstatus')):
                # extract multiValueQueryStringParameters

                statusCode, body = self.jpeGetJobForStatus(event)
                return statusCode, body

        else:
            statusCode = 404
            body = "Not Yet Implemented"

        return statusCode, body


    def ListItems(self, event):
        sub_list = []
        list_out = []

        prefix = self.iss + '/' + self.sub + os.path.dirname(self.path) + '/data'
        print("prefix : " + prefix)
        # iterate objects in the sub folder and print
        result = self.s3_client.list_objects(Bucket=self.root_bucket, Prefix=prefix)
        if 'Contents' in result:
            for key in result['Contents']:
                if not key['Key'].endswith('/'):
                    print(key['Key'])
                    # TODO: make this more configurable / dynamic
                    list_out.append('https://s3-eu-west-1.amazonaws.com/' + self.root_bucket + '/' + key['Key'])

        return list_out


    def extractMultiValueQueryString(self, event, key):

        queryList = []

        if event['multiValueQueryStringParameters'] is not None:
            if key in event['multiValueQueryStringParameters']:
                for id in event['multiValueQueryStringParameters'][key]:
                    queryList.append(id)
                return queryList
            else:
                return None
        else:
            return None

    def extractQueryStrings(self, event, key):

        if event['queryStringParameters'] is not None:
            if key in event['queryStringParameters']:
                return event['queryStringParameters'][key]
            else:
                return None
        else:
            return None


    def jpeGetJobForStatus(self, event):

        data = {}
        jobIDList = []

        data['jobStatusList'] = [ "initialised","cancelled","failed","pending","timeout","success", "completed" ]
        tmp = json.dumps(data)
        json_data = json.loads(tmp)

        # check for multiple jobIDs
        jobIDList = self.extractMultiValueQueryString(event, 'jobID')

        # check for single jobID
        if jobIDList is None:
            jobIDList = self.extractQueryStrings(event, 'jobID')
            if jobIDList is None:
                return 400, "no jobIDs provided"
        for id in jobIDList:
            print("jobIDList : " + str(id))

        # get the status of all jobs in kx system
        result = self.session.makeJpeRequest("connect", "getJobForStatus", json_data)

        # check for specific jobIDs we want
        returnData = []
        knownJobs = []
        for jobStatusResult in result.json()['msg']:
            knownJobs.append(jobStatusResult['jobID'])
            if jobStatusResult['jobID'] in jobIDList:
                returnItem = {'jobID': jobStatusResult['jobID'], 'job': jobStatusResult['job'], 'status': jobStatusResult['status']}
                returnData.append(returnItem)

        for jobID in jobIDList:
            if jobID not in knownJobs:
                returnItem = {'jobID': jobID, 'job': 'unknown', 'status': 'job not listed in system'}
                returnData.append(returnItem)

        return result.status_code, returnData


    def getDatasetMeta(self, event):

            
        if self.path.startswith("/dev"):
            print("self.path[3:] : " + self.path[4:])
            self.path= self.path[4:]
            
        print("self.path : " + self.path)
        print("dirname : " + os.path.dirname(self.path))
        
        
        
        prefix = self.iss + '/' + self.username + os.path.dirname(self.path) + '/' + self.dataset_name + '.json'
        #prefix = self.iss + '/' + self.sub + os.path.dirname(self.path) + '/' + self.dataset_name + '.json'
        print("prefix : " + prefix)

        s3 = boto3.resource('s3')

        obj = s3.Object(self.root_bucket, prefix)
        dataset_metadata = obj.get()['Body'].read().decode('utf-8')

        return json.loads(dataset_metadata)


    def setupKxSession(self, event):
        if self.kx_controller is not None:
            self.session = kxsession(self.kx_controller)



    def jpeIngest(self, event, item):

        print("in jpeIngest")


        if self.dataset_name is None:
            self.dataset_name = event['pathParameters']['datasetname']
            self.catID = self.username + '_' + self.dataset_name

        # check for multiple jobIDs
        zoomlevels = self.extractMultiValueQueryString(event, 'zoomlevels')
        
        if zoomlevels is None:
            zoomlevels = [12,13]
            
        # check for single jobID
        for z in zoomlevels:
            print("zoomlevel : " + str(z))
            #if 3 < z < 20:
            #    print("Unsupported zoom level : " + str(x))
            #    return 400, {'status': 'Unsupported zoom level' + str(x) }

        self.zoomlevels = ','.join(str(x) for x in zoomlevels)


        # build up parameters for dataset creation based on dataset metadata file
        data = {}
        dataset_metadata = self.getDatasetMeta(event)
        data['catID'] = self.catID

        if 'dataset_type' in dataset_metadata:
            data['dataSrc'] = dataset_metadata['dataset_type']
        else:
            return 404, "required dataset_type not in dataset metadata"

        if 'dataset_srcs' in dataset_metadata:
            data['sceneData'] = ','.join(str(x) for x in dataset_metadata['dataset_srcs'])
        else:
            return 404, "required dataset_srcs not in dataset metadata"

        if 'dataset_bands' in dataset_metadata:
            if (dataset_metadata['dataset_type'] == 'customImagery') and (len(dataset_metadata['dataset_bands']) == 3):
                self.bands = data['bands'] = ','.join(str(x) for x in dataset_metadata['dataset_bands'])
            if (dataset_metadata['dataset_type'] == 'customElevation') and (len(dataset_metadata['dataset_bands']) == 1):
                self.bands = data['bands'] = dataset_metadata['dataset_bands']
        else:
            if dataset_metadata['dataset_type'] == 'customImagery':
                self.bands = data['bands'] = "0,1,2"
            if dataset_metadata['dataset_type'] == 'customElevation':
                self.bands = data['bands'] = "0"

        tmp = json.dumps(data)
        json_data = json.loads(tmp)


        print(json_data)

        # add dataset to the catalogue
        result = self.session.makeJpeRequest("jt", "createDataset", json_data)

        data = {}

        # Set up the scene list job
        data['jobTemp'] = "getSceneListJT"
        data['jobParams'] =  "".join(["(enlist `catID)!enlist \"", self.catID, "\""])
        data['jobTarget'] =  "cgJobEngine"
        data['jobPriority'] = "1"

        tmp = json.dumps(data)
        json_data = json.loads(tmp)

        result = self.session.makeJpeRequest("connect", "scheduleJob", json_data)
        self.sceneListDepID = result.json()['msg'][0]['Long']

        # Set up the ingestion job
        data['jobTemp'] = "ingestJT"
        data['jobDepends'] = [int(self.sceneListDepID)]

        tmp = json.dumps(data)
        json_data = json.loads(tmp)

        result = self.session.makeJpeRequest("connect", "scheduleJob", json_data)
        self.ingestDepID = result.json()['msg'][0]['Long']

        print ("response     : " + result.text)
        print ("status       : " + str(result.status_code))

        return result.status_code, result.text



    def jpeTile(self, event):

        print("in jpeTile")

        data = {}
        data['catID'] = self.catID
        data['tilingID'] = self.tilingJobUUID = self.catID + "_" + str(uuid.uuid4());
        data['zoomLevel'] = self.zoomlevels
        data['bands'] = self.bands
        data['saveLocType'] = self.saveLocType # hardcoded until we support other typed
        if self.boundBox is not None:
            data['boundBoxOverride'] = self.boundBox
        data['saveLoc'] = '/' + self.root_bucket + '/' + self.iss + '/' + self.sub + os.path.dirname(self.path) + '/tiles'

        tmp = json.dumps(data)
        json_data = json.loads(tmp)

        print(json_data)

        # add dataset to the catalogue
        result = self.session.makeJpeRequest("jt", "createTiling", json_data)

        data = {}
        data['jobTemp'] = "tileJT"
        data['jobParams'] =  "".join(["(enlist `catID)!enlist \"", self.catID, "\""])
        data['jobTarget'] =  "cgJobEngine"
        data['jobDepends'] = [int(self.ingestDepID)]
        data['jobPriority'] = "1"

        tmp = json.dumps(data)
        json_data = json.loads(tmp)

        result = self.session.makeJpeRequest("connect", "scheduleJob", json_data)
        self.tilingDepID = result.json()['msg'][0]['Long']

        print ("response: " + result.text)
        print ("status: " + str(result.status_code))

        return result.status_code, result.text


    def jpePurge(self, event):

        print("in jpePurge")

        data = {}
        data['jobTemp'] = "purgeDatasetJT"
        data['jobParams'] =  self.dataset_name
        data['jobTarget'] =  "cgJobEngine"
        if self.tilingDepID is not None:
            data['jobDepends'] = [int(self.tilingDepID)]
        data['jobPriority'] = "1"

        tmp = json.dumps(data)
        json_data = json.loads(tmp)

        result = self.session.makeJpeRequest("connect", "scheduleJob", json_data)
        self.purgeDepID = result.json()['msg'][0]['Long']

        print ("response: " + result.text)
        print ("status: " + str(result.status_code))

        return result.status_code, result.text