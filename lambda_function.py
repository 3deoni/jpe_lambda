import json
import boto3
import base64
import api_handlers
import os


def lambda_handler(event, context):
    #print (event)
    print ("\n---------------------------------\n")
    print("entry point")
    # Should get root bucket from environment variable instead
    if 'DATA_S3_BUCKET' in os.environ:
        root_bucket = os.environ['DATA_S3_BUCKET']
    else:
        root_bucket = '3deo-visualizer-resources'

    print("calling api_handlers")
    handler = api_handlers.api_handler(event, root_bucket)
    print("returned from api_handlers")

    response = handler.get_response()
    print(response)
    return response

