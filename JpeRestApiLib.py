import requests
import uuid
#import md5
#from hashlib import md5
import hashlib
from datetime import datetime
import json
from base64 import b64encode
from Crypto.Hash import HMAC
from Crypto.Hash import SHA
import urllib3

# Request Authorization token from JME

class kxsession:
    def __init__(self, http_host='http://localhost:8080'):
        self.http_host = http_host
        self.base_url = self.http_host + '/connect/api'
        self.login_url = self.base_url + '/auth/login'
        self.username = 'connectAPIUser'
        self.password = 'letConnectIn'
        print("in __init__")

    def auth(self):

        UUID_STR = str(uuid.uuid4())
        print("in auth")
        print("uuid str : " + UUID_STR)

        LOGIN_DATA = {
            "msg" : [{
                "username" : self.username,
                "password" : self.password
            }],
            "type" : "LoginReq",
            "id": UUID_STR,
            "date" : datetime.now().strftime('%a, %d %b %Y %H:%M:%S ') + 'GMT'
        }

        response=requests.post(url=self.login_url, verify=False, data=json.dumps(LOGIN_DATA))

        if response.status_code != 200:
            print ("Login failed. Received response: " + str(response.status_code))

        print ("Data" + response.text)
        print (type(response.text))

        # Got successful login
        responseData=json.loads(response.text)

        return response.status_code, responseData



    def setupRequest(self, dateStr, authPath, QUERY_DATA):
        #queryMd5Str = md5.new(json.dumps(QUERY_DATA)).hexdigest()
        queryMd5Str = hashlib.md5(json.dumps(QUERY_DATA).encode()).hexdigest()
        print("queryMd5Str : " + queryMd5Str + "is of type :")

        status_code, Auth_responseData = self.auth()
        print("sessionId :" + Auth_responseData['msg'][0]['sessionId'])

        #sessionId = Auth_responseData['msg'][0]['sessionId'].encode('utf-8')
        sessionId = Auth_responseData['msg'][0]['sessionId']
        print("sessionId : " + sessionId)
        print(type(sessionId))

        #authPath       - string
        #self.username  - string
        #queryMd5Str    - string
        #mimeHeader     - string
        #dateStr        - string
        #sessionId      - string

        mimeHeader = 'application/json'
        stringToSign = "POST\n" + authPath + "\n" + self.username + "\n" + queryMd5Str +  \
            "\n" + mimeHeader + \
            "\n" + dateStr + \
            "\n" + sessionId

        hmacEncStr = HMAC.new( \
            sessionId.encode(), \
            stringToSign.encode(), \
            SHA).digest()

        b64Enc = b64encode(hmacEncStr)

        authorization = self.username + \
            sessionId[len(sessionId)-5:len(sessionId)] + \
            ":" + b64Enc.decode()

        return authorization


    def makeJpeRequest(self, MethodGroup, MethodName, MethodParams):

        requestPath = self.base_url + "/" + MethodGroup + "/" + MethodName

        UUID_STR = str(uuid.uuid4())

        dateStr=datetime.now().strftime('%a, %d %b %Y %H:%M:%S ') + 'GMT'

        QueryData = {
            "type" : MethodName+"Req",
            "msg" : [MethodParams],
            "id" : UUID_STR,
            "date" : dateStr
        }

        authPath = "/connect/api/" + MethodGroup + "/" + MethodName
        print("authPath : " + authPath)
        print ("requestPath: " + requestPath)
        print("Query : " + json.dumps(QueryData))


        authorization = self.setupRequest(dateStr, authPath, QueryData)
        print ("authorization: " + str(authorization ))

        headers = {
            "content-type" : "application/json",
            "Date" : dateStr,
            "authorization" : authorization
        }

        queryResp=requests.post(url=requestPath, verify=False, data=json.dumps(QueryData), headers=headers)


        return queryResp